# Network Based Project

Repository for my Network Based Data Analysis project. Master in Quantitative and Computational Biology 2018/2019.

The project is focused on the analysis of the dataset [found on GEO](https://www.ncbi.nlm.nih.gov/geo/query/acc.cgi?acc=GSE53987) of microarray profiles taken from post mortem patients affected by _depression_, _schizophrenia_ and _bipolar disorder_.

./Rhistory is not provided due to its size.
